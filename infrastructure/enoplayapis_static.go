package infrastructure

import (
	"bytes"
	"fmt"
	"net/http"
)

type EnoplayStaticOptions struct {
	ApiBase string
}

type EnoplayStaticHandler struct {
	options *EnoplayStaticOptions
}

func NewEnoplayStaticHandler(options EnoplayStaticOptions) *EnoplayStaticHandler {
	handler := &EnoplayStaticHandler{
		options: &options,
	}
	return handler
}

func (handler *EnoplayStaticHandler) Delete(sessionToken string, uid string) error {
	var jsonBody = []byte(fmt.Sprintf(`{}`))
	req, err := http.NewRequest("DELETE", fmt.Sprintf("%v/images/%v", handler.options.ApiBase, uid), bytes.NewBuffer(jsonBody))
	if err != nil {
		return fmt.Errorf("Error creating delete request from Enoplay Static: %v", err)
	}
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %v", sessionToken))
	req.Header.Set("Content-Type", "application/json")
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return fmt.Errorf("Error creating delete response from Enoplay Static: %v", err)
	}
	defer resp.Body.Close()
	return nil
}
