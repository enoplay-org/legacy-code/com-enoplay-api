package usecase

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"regexp"
	"strings"
	"unicode"
	"unicode/utf8"

	govalidator "gopkg.in/asaskevich/govalidator.v4"

	"github.com/twinj/uuid"
	"gitlab.com/enoplay/com-enoplay-api/domain"
	"golang.org/x/crypto/bcrypt"
)

type UserInteractor struct {
	UserRepository domain.UserRepository
	GameRepository domain.GameRepository
}

var vanityUsernames map[string]bool
var defaultUserDescriptions []string

func NewUserInteractor() *UserInteractor {
	// Retrieve vanity names
	vanityUsernamesBytes, err := ioutil.ReadFile("usecase/vanity_usernames.json")
	if err != nil {
		log.Fatal(fmt.Errorf("Error loading vanity_usernames: %v", err.Error()))
	}

	err = json.Unmarshal(vanityUsernamesBytes, &vanityUsernames)
	if err != nil {
		log.Fatal(fmt.Errorf("Error parsing vanity usernames: %v", err.Error()))
	}

	// Retrieve default descriptions
	defaultDescriptionsBytes, err := ioutil.ReadFile("usecase/default_user_descriptions.json")
	if err != nil {
		log.Fatal(fmt.Errorf("Error loading default_user_descriptions: %v", err.Error()))
	}

	err = json.Unmarshal(defaultDescriptionsBytes, &defaultUserDescriptions)
	if err != nil {
		log.Fatal(fmt.Errorf("Error parsing default user descriptions: %v", err.Error()))
	}

	return &UserInteractor{}
}

// Get returns a User by their uid
func (interactor *UserInteractor) Get(uid string) (domain.User, error) {
	return interactor.UserRepository.Get(uid)
}

// GetByUsername gets a User by username
func (interactor *UserInteractor) GetByUsername(username string) (domain.User, error) {
	return interactor.UserRepository.GetByUsername(username)
}

// ListUsers returns a list of Users
func (interactor *UserInteractor) ListUsers(field string, query string, lastID string, limit int, sort string) domain.Users {
	return interactor.UserRepository.Filter(field, query, lastID, limit, sort)
}

// Create creates a new User
func (interactor *UserInteractor) Create(email, username, fullname, password string) (domain.User, error) {
	if !isValidEmail(email) {
		return domain.User{}, fmt.Errorf("Invalid Email")
	}

	if uMinLength, uMaxLength, uAlphaNum, uNonVanity := isValidUsername(username); !(uMinLength && uMaxLength && uAlphaNum && uNonVanity) {
		if !uMinLength {
			return domain.User{}, fmt.Errorf("Username must have at least 1 character")
		} else if !uMaxLength {
			return domain.User{}, fmt.Errorf("Username must have less than 16 characters")
		} else if !uAlphaNum {
			return domain.User{}, fmt.Errorf("Username must contain alphanumerical characters or _")
		} else if !uNonVanity {
			return domain.User{}, fmt.Errorf("Invalid Username")
		}

		return domain.User{}, fmt.Errorf("Invalid Username")
	}

	if aMinLength, aMaxLength := isValidAlias(fullname); !(aMinLength && aMaxLength) {
		if !aMinLength {
			return domain.User{}, fmt.Errorf("Full name must have at least 1 character")
		} else if !aMaxLength {
			return domain.User{}, fmt.Errorf("Full name must have less than 20 characters")
		}

		return domain.User{}, fmt.Errorf("Invalid Full Name")
	}

	if pLength, pLetter, pNumber, pUppercase, pSpecial := isValidPassword(password); !(pLength && pLetter && pNumber && pUppercase && pSpecial) {
		if !pLength {
			return domain.User{}, fmt.Errorf("Password must contain at least 7 characters")
		} else if !pLetter {
			return domain.User{}, fmt.Errorf("Password must contain at least 1 letter")
		} else if !pNumber {
			return domain.User{}, fmt.Errorf("Password must contain at least 1 number")
		} else if !pUppercase {
			return domain.User{}, fmt.Errorf("Password must contain at least 1 uppercase character")
		} else if !pSpecial {
			return domain.User{}, fmt.Errorf("Password must contain at least 1 special character")
		}

		return domain.User{}, fmt.Errorf("Invalid Password")
	}

	// Ensure Email is available
	if !interactor.isAvailableEmail(email) {
		return domain.User{}, fmt.Errorf("Email is taken")
	}

	// Ensure Username is available
	if !interactor.isAvailableUsername(username) {
		return domain.User{}, fmt.Errorf("Username is taken")
	}

	// Setup default media
	media := domain.UserMedia{}
	media.Icon.UID = fmt.Sprintf("%v-%v", "default-icon", username)
	media.Icon.Source = generateDefaultUserIcon(string([]rune(fullname)[0]))
	media.Banner.UID = fmt.Sprintf("%v-%v", "default-banner", username)
	media.Banner.Source = generateDefaultUserBanner(fullname)

	// Setup hashed password and email confirmation code
	hashedPassword, _ := hashPassword(password)
	emailConfirmationCode := generateEmailConfirmationCode()

	description := generateDefaultUserDescription(fullname)

	user := domain.User{
		Email:                 strings.TrimSpace(email),
		Username:              strings.TrimSpace(username),
		Alias:                 strings.TrimSpace(fullname),
		Description:           description,
		Media:                 media,
		Status:                domain.UserStatusPending,
		HashedPassword:        hashedPassword,
		EmailConfirmationCode: emailConfirmationCode,
	}

	return interactor.UserRepository.Create(user)
}

// UpdateAlias updates a User's alias
func (interactor *UserInteractor) UpdateAlias(alias string, user domain.User) (domain.User, error) {
	if alias == user.Alias {
		return domain.User{}, nil // No need to update with the same alias
	}

	if aMinLength, aMaxLength := isValidAlias(alias); !(aMinLength && aMaxLength) {
		if !aMinLength {
			return domain.User{}, fmt.Errorf("Full name must have at least 1 character")
		} else if !aMaxLength {
			return domain.User{}, fmt.Errorf("Full name must have less than 20 characters")
		}

		return domain.User{}, fmt.Errorf("Invalid Full Name")
	}

	user.Alias = alias

	return interactor.UserRepository.UpdateAlias(user)
}

// UpdateUsername updates a User's username
func (interactor *UserInteractor) UpdateUsername(username string, user domain.User) (domain.User, error) {
	if uMinLength, uMaxLength, uAlphaNum, uNonVanity := isValidUsername(username); !(uMinLength && uMaxLength && uAlphaNum && uNonVanity) {
		if !uMinLength {
			return domain.User{}, fmt.Errorf("Username must have at least 1 character")
		} else if !uMaxLength {
			return domain.User{}, fmt.Errorf("Username must have less than 16 characters")
		} else if !uAlphaNum {
			return domain.User{}, fmt.Errorf("Username must contain alphanumerical characters or _")
		} else if !uNonVanity {
			return domain.User{}, fmt.Errorf("Invalid Username")
		}

		return domain.User{}, fmt.Errorf("Invalid Username")
	}

	if !interactor.isAvailableUsername(username) {
		return domain.User{}, fmt.Errorf("Username is taken")
	}

	user.Username = username

	return interactor.UserRepository.UpdateUsername(user)
}

// UpdateEmail updates a User's email
func (interactor *UserInteractor) UpdateEmail(email string, user domain.User) (domain.User, error) {
	if email == user.Email {
		return domain.User{}, nil // No need to update with the same email
	}

	if !isValidEmail(email) {
		return domain.User{}, fmt.Errorf("Invalid Email")
	}

	if !interactor.isAvailableEmail(email) {
		return domain.User{}, fmt.Errorf("Email is taken")
	}

	user.Email = email
	user.EmailConfirmationCode = generateEmailConfirmationCode()

	return interactor.UserRepository.UpdateEmail(user)
}

// UpdateDescription updates a User's description
func (interactor *UserInteractor) UpdateDescription(description string, user domain.User) (domain.User, error) {
	if description == user.Description {
		return domain.User{}, nil // No need to update with the same description
	}

	if !isValidUserDescription(description) {
		return domain.User{}, fmt.Errorf("Invalid Description")
	}

	user.Description = description

	return interactor.UserRepository.UpdateDescription(user)
}

// UpdatePassword updates a User's password
func (interactor *UserInteractor) UpdatePassword(oldPassword string, newPassword string, user domain.User) error {
	if !hasValidPassword(user.HashedPassword, oldPassword) {
		return fmt.Errorf("Old password is invalid")
	}

	if pLength, pLetter, pNumber, pUppercase, pSpecial := isValidPassword(newPassword); !(pLength && pLetter && pNumber && pUppercase && pSpecial) {
		if !pLength {
			return fmt.Errorf("New Password must contain at least 7 characters")
		} else if !pLetter {
			return fmt.Errorf("New Password must contain at least 1 letter")
		} else if !pNumber {
			return fmt.Errorf("New Password must contain at least 1 number")
		} else if !pUppercase {
			return fmt.Errorf("New Password must contain at least 1 uppercase character")
		} else if !pSpecial {
			return fmt.Errorf("New Password must contain at least 1 special character")
		}

		return fmt.Errorf("New Password is invalid")
	}

	hashedPassword, err := hashPassword(newPassword)
	if err != nil {
		return err
	}

	user.HashedPassword = hashedPassword

	return interactor.UserRepository.UpdatePassword(user)
}

// ResetEmailConfirmationCode sends an email confirmation code to a User's email
func (interactor *UserInteractor) ResetEmailConfirmationCode(user domain.User) error {
	emailConfirmationCode := generateEmailConfirmationCode()
	user.EmailConfirmationCode = emailConfirmationCode
	return interactor.UserRepository.UpdateEmailConfirmationCode(user)
}

// ConfirmEmail uses an email confirmation code to activate a User's profile
func (interactor *UserInteractor) ConfirmEmail(code string) error {
	if !isValidEmailConfirmationCode(code) {
		return fmt.Errorf("Invalid confirmation code")
	}
	user, err := interactor.UserRepository.FindByEmailConfirmationCode(code)
	if err != nil {
		return fmt.Errorf("Invalid confirmation code")
	}

	if user.EmailConfirmationCode != code {
		return fmt.Errorf("Invalid confirmation code")
	}

	return interactor.UserRepository.ConfirmEmail(user)
}

// ResetPassword sends a password reset email to a User
func (interactor *UserInteractor) ResetPassword(email string) error {
	if !isValidEmail(email) {
		return fmt.Errorf("Invalid email")
	}

	user, err := interactor.UserRepository.FindByEmail(email)
	if err != nil {
		return fmt.Errorf("Invalid email: %v", err.Error())
	}

	user.PasswordResetCode = generatePasswordResetCode()

	return interactor.UserRepository.ResetPassword(user)
}

// UpdatePasswordWithCode uses a password reset code to override the existing User password
func (interactor *UserInteractor) UpdatePasswordWithCode(code, password string) error {
	if !isValidPasswordResetCode(code) {
		return fmt.Errorf("Invalid reset code")
	}
	user, err := interactor.UserRepository.FindByPasswordResetCode(code)
	if err != nil {
		return fmt.Errorf("Invalid reset code")
	}

	if user.PasswordResetCode != code {
		return fmt.Errorf("Invalid reset code")
	}

	if pLength, pLetter, pNumber, pUppercase, pSpecial := isValidPassword(password); !(pLength && pLetter && pNumber && pUppercase && pSpecial) {
		if !pLength {
			return fmt.Errorf("Password must contain at least 7 characters")
		} else if !pLetter {
			return fmt.Errorf("Password must contain at least 1 letter")
		} else if !pNumber {
			return fmt.Errorf("Password must contain at least 1 number")
		} else if !pUppercase {
			return fmt.Errorf("Password must contain at least 1 uppercase character")
		} else if !pSpecial {
			return fmt.Errorf("Password must contain at least 1 special character")
		}

		return fmt.Errorf("Invalid Password")
	}

	hashedPassword, _ := hashPassword(password)
	user.HashedPassword = hashedPassword

	return interactor.UserRepository.UpdatePassword(user)
}

// SendFeedback sends a feedback message
func (interactor *UserInteractor) SendFeedback(name, email, message string) error {
	name = strings.TrimSpace(name)
	if name == "" {
		return fmt.Errorf("Invalid name")
	}

	if !isValidEmail(email) {
		return fmt.Errorf("Invalid email")
	}

	message = strings.TrimSpace(message)
	if message == "" {
		return fmt.Errorf("Invalid message")
	}

	return interactor.UserRepository.SendFeedback(name, email, message)
}

// Delete destroys a User
func (interactor *UserInteractor) Delete(sessionToken string, user domain.User) error {

	// Delete all published Games
	gamePublishHistory, err := interactor.UserRepository.GetGamePublishHistory(user.UID)
	if err != nil {
		return fmt.Errorf("Error retrieving user's game publish history: %v", err)
	}

	for _, publishedItem := range gamePublishHistory.History {
		game, _ := interactor.GameRepository.Get(publishedItem.GameUID)
		interactor.GameRepository.Delete(sessionToken, game)
	}

	// Delete User
	return interactor.UserRepository.Delete(sessionToken, user)
}

// GetGamePublishHistory returns a list of public Games pulished by a User
func (interactor *UserInteractor) GetGamePublishHistory(user domain.User, isPublisher bool) (map[string]domain.UserGamePublishHistoryItem, error) {
	gamePublishHistory := make(map[string]domain.UserGamePublishHistoryItem)

	// Retrieve Game publish history
	userGamePublishHistory, err := interactor.UserRepository.GetGamePublishHistory(user.UID)
	if err != nil {
		return gamePublishHistory, fmt.Errorf("Error retrieving Game publish history: %v", err)
	}

	// Return all Games if User is publisher
	if isPublisher {
		return userGamePublishHistory.History, nil
	}

	// Search for public Games
	for gameUID, gameItem := range userGamePublishHistory.History {
		game, err := interactor.GameRepository.Get(gameUID)
		if err != nil {
			continue
		}

		if game.Privacy == domain.GamePrivacyPublic {
			gamePublishHistory[gameUID] = gameItem
		}
	}
	return gamePublishHistory, nil
}

// PurchaseGame initiates a purchase transaction of a Game
func (interactor *UserInteractor) PurchaseGame(user domain.User, game domain.Game, sourceToken string) error {
	isUserBanned := interactor.GameRepository.IsUserBanned(game.UID, user.UID)
	if isUserBanned {
		return fmt.Errorf("User is banned from the game")
	}

	gamePlayAccessItem, err := interactor.UserRepository.PurchaseGame(user, game, sourceToken)
	if err != nil {
		return fmt.Errorf("Error purchasing game: %v", err)
	}

	gameUserPurchaseItem := domain.GameUserPurchaseHistoryItem{
		UserUID:   user.UID,
		DateAdded: gamePlayAccessItem.DateAdded,
		Receipt:   gamePlayAccessItem.Receipt,
	}

	// Updaate purchase history of game
	err = interactor.GameRepository.UpdateUserPurchaseHistory(game.UID, gameUserPurchaseItem)
	if err != nil {
		return fmt.Errorf("Error updating the purchase history of game: %v", err)
	}

	return nil
}

// VerifyGamePlayAccess checks if an authenticated User has been authorized access to play a Game
func (interactor *UserInteractor) VerifyGamePlayAccess(user domain.User, game domain.Game) (bool, string, error) {
	if game.Price.IsFree() {
		return true, fmt.Sprintf("Everyone can play :)"), nil
	}

	if game.Publisher.UID == user.UID {
		return true, fmt.Sprintf("Publisher can play their own game"), nil
	}

	hasPlayAccess := interactor.UserRepository.HasPlayAccess(user.UID, game.UID)
	if !hasPlayAccess {
		return false, fmt.Sprintf("User doesn't have access to this game"), nil
	}

	return true, fmt.Sprintf("User has access to this game"), nil
}

// VerifyAnonGamePlayAccess checks if an anonyous User has authority to play a Game
func (interactor *UserInteractor) VerifyAnonGamePlayAccess(game domain.Game) (bool, string, error) {
	if game.Price.IsFree() {
		return true, fmt.Sprintf("Everyone can play :)"), nil
	}

	return false, fmt.Sprintf("User doesn't have access to this game"), nil
}

// VerifyGameEditAccess checks if a User has authority to edit a Game
func (interactor *UserInteractor) VerifyGameEditAccess(user domain.User, game domain.Game) (bool, string, error) {
	if game.Publisher.UID == user.UID {
		return true, fmt.Sprintf("Publisher can edit their own game"), nil
	}

	return false, fmt.Sprintf("User doesn't have access to this game"), nil
}

func generateDefaultUserDescription(name string) string {
	descritpion := defaultUserDescriptions[rand.Intn(len(defaultUserDescriptions))]
	descritpion = strings.Replace(descritpion, "{{ name }}", name, -1)
	return descritpion
}

var defaultUserIconColors = []string{"FF6344", "3D4E5E", "22C19A"}

func generateDefaultUserIcon(letter string) string {
	color := defaultUserIconColors[rand.Intn(len(defaultUserIconColors))]
	return fmt.Sprintf("https://www.dummyimage.com/128x128/%v/F7F7F8.png&text=%v", color, letter)
}

func generateDefaultUserBanner(name string) string {
	return fmt.Sprintf("https://www.dummyimage.com/2120x352/000/ffffff&text=%v", name)
}

func generateEmailConfirmationCode() string {
	return uuid.NewV4().String()
}

func generatePasswordResetCode() string {
	return uuid.NewV4().String()
}

func hashPassword(password string) (string, error) {
	passwordBytes := []byte(password)

	hashedPassword, err := bcrypt.GenerateFromPassword(passwordBytes, bcrypt.DefaultCost)
	if err != nil {
		return "", err
	}

	return string(hashedPassword), nil
}

func (interactor *UserInteractor) isAvailableEmail(email string) bool {
	return !interactor.UserRepository.ExistsByEmail(email)
}

func (interactor *UserInteractor) isAvailableUsername(username string) bool {
	return !interactor.UserRepository.ExistsByUsername(username)
}

func isValidEmailConfirmationCode(code string) bool {
	return utf8.RuneCountInString(code) > 5
}

func isValidPasswordResetCode(code string) bool {
	return utf8.RuneCountInString(code) > 5
}

func isValidEmail(email string) bool {
	return govalidator.IsEmail(email)
}

func isValidUsername(username string) (minLength, maxLength, alphaNum, nonVanity bool) {
	username = strings.TrimSpace(username)
	length := utf8.RuneCountInString(username)
	re := regexp.MustCompile(`^[a-zA-Z0-9_]+$`)
	return length > 0, length < 16, re.MatchString(username), !vanityUsernames[username]
}

func isValidAlias(alias string) (minLength, maxLength bool) {
	alias = strings.TrimSpace(alias)
	length := utf8.RuneCountInString(alias)
	return length > 0, length < 21
}

func isValidUserDescription(description string) bool {
	return true
}

func isValidPassword(password string) (length, letter, number, uppercase, special bool) {
	length = utf8.RuneCountInString(password) >= 5
	special = true
	uppercase = true
	for _, c := range password {
		switch {
		case unicode.IsNumber(c):
			number = true
			/*
				case unicode.IsUpper(c):
					uppercase = true
				case unicode.IsPunct(c) || unicode.IsSymbol(c):
					special = true
			*/
		case unicode.IsLetter(c) || c == ' ':
			letter = true
		default:
			return
		}
	}
	return
}
