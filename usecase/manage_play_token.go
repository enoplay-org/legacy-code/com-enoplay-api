package usecase

import (
	"fmt"
	"math/rand"

	"github.com/twinj/uuid"

	"gitlab.com/enoplay/com-enoplay-api/domain"
)

type PlayTokenInteractor struct {
	PlayTokenRepository domain.PlayTokenRepository
	UserRepository      domain.UserRepository
	GameRepository      domain.GameRepository
}

func NewPlayTokenInteractor() *PlayTokenInteractor {
	return &PlayTokenInteractor{}
}

func (interactor *PlayTokenInteractor) Verify(token, gameUID string) (domain.User, bool, error) {
	playTokenClaim, err := interactor.PlayTokenRepository.Verify(token)
	if err != nil {
		return domain.User{}, true, err
	}

	// Ensure token is for specified game
	if playTokenClaim.GameUID != gameUID {
		return domain.User{}, true, fmt.Errorf("Invalid play token")
	}

	// Return fake user for anon user tokens
	if playTokenClaim.IsAnon {
		return newAnonUser(playTokenClaim.UserUID), true, nil
	}

	user, err := interactor.UserRepository.Get(playTokenClaim.UserUID)
	return user, false, err
}

func (interactor *PlayTokenInteractor) Create(user domain.User, game domain.Game) (string, error) {
	if game.Privacy == domain.GamePrivacyPrivate && user.UID != game.Publisher.UID {
		return "", fmt.Errorf("User is unauthorized")
	}

	isUserBanned := interactor.GameRepository.IsUserBanned(game.UID, user.UID)
	if isUserBanned {
		return "", fmt.Errorf("User is banned")
	}

	// Ensure user has access to game (e.g. purchase or gift) if game requires payment
	if !game.Price.IsFree() && game.Publisher.UID != user.UID {
		hasPlayAccess := interactor.UserRepository.HasPlayAccess(user.UID, game.UID)
		if !hasPlayAccess {
			return "", fmt.Errorf("User doesn't have access to this game")
		}
	}

	// Create Play Token
	tokenClaim := domain.PlayTokenClaim{
		GameUID: game.UID,
		UserUID: user.UID,
		IsAnon:  false,
	}
	token, err := interactor.PlayTokenRepository.Create(tokenClaim)
	if err != nil {
		return "", fmt.Errorf("Error creating play token: %v", err.Error())
	}

	return token, nil
}

func (interactor *PlayTokenInteractor) CreateAnon(game domain.Game) (string, error) {
	// Anon users can only access free games
	if !game.Price.IsFree() {
		return "", fmt.Errorf("User doesn't have access to this game")
	}

	var anonUserUID = uuid.NewV4().String()

	// Create Play Token
	tokenClaim := domain.PlayTokenClaim{
		GameUID: game.UID,
		UserUID: anonUserUID,
		IsAnon:  true,
	}
	return interactor.PlayTokenRepository.CreateAnon(tokenClaim)
}

func newAnonUser(userUID string) domain.User {
	var username = fmt.Sprintf("anon_%v", rand.Intn(100000))
	var user = domain.User{
		UID:      userUID,
		Username: username,
		Alias:    username,
	}
	return user
}
