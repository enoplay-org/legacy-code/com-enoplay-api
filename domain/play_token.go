package domain

import "time"

type PlayTokenRepository interface {
	Verify(tokenString string) (PlayTokenClaim, error)
	Create(claim PlayTokenClaim) (string, error)
	CreateAnon(claim PlayTokenClaim) (string, error)
}

type PlayTokenClaim struct {
	UserUID     string    `json:"userUID" bson:"userUID"`
	GameUID     string    `json:"gameUID" bson:"gameUID"`
	IsAnon      bool      `json:"isAnon"`
	DateIssued  time.Time `json:"dateIssued" bson:"dateIssued"`
	DateExpired time.Time `json:"dateExpired" bson:"dateExpired"`
	JTI         string    `json:"jti" bson:"jti"`
}

func NewPlayTokenClaim(userUID, gameUID string) PlayTokenClaim {
	return PlayTokenClaim{UserUID: userUID, GameUID: gameUID}
}
