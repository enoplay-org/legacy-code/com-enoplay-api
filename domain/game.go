package domain

import "time"

type GameRepository interface {
	Get(uid string) (Game, error)
	GetByGID(gid string) (Game, error)
	GetAll() Games
	Filter(queryList map[string]interface{}, limit int, sort string) Games

	Create(game Game) (Game, error)
	UpdateTitle(game Game) (Game, error)
	UpdateDescription(game Game) (Game, error)
	UpdatePrice(game Game) (Game, error)
	UpdatePrivacy(game Game) (Game, error)
	Delete(sessionToken string, game Game) error
	DeleteThumbnail(sessionToken string, game Game) (Game, error)
	DeleteImages(sessionToken string, game Game) (Game, error)
	// Count(field string, query string) int
	ExistsByGID(gid string) bool

	UpdateSystemBrowserSupport(game Game) (Game, error)

	IsUserBanned(gameUID string, userUID string) bool
	UpdateUserPurchaseHistory(gameUID string, purchase GameUserPurchaseHistoryItem) error
	DeleteApp(sessionToken string, game Game) error
}

// Game is a Game
type Game struct {
	UID    string `json:"uid" bson:"uid,omitempty"`
	GID    string `json:"gid,omitempty" bson:"gid"`
	Status string `json:"status,omitempty" bson:"status"`

	Title       string `json:"title,omitempty" bson:"title"`
	Description string `json:"description,omitempty" bson:"description"`

	// Accessibility
	Tags    []string  `json:"tags,omitempty" bson:"tags"`
	Privacy string    `json:"privacy,omitempty" bson:"privacy"`
	Price   GamePrice `json:"price" bson:"price"`

	// Media
	Media GameMedia `json:"media,omitempty" bson:"media"`

	// System Support
	SystemSupport GameSystemSupport `json:"systemSupport,omitempty" bson:"systemSupport"`

	// Controls
	Controls GameControls `json:"controls,omitempty"`

	// Language Support
	LanguageSupport GameLanguageSupport `json:"languageSupport,omitempty" bson:"languageSupport"`

	// Statistics
	Stats struct {
		TotalPlays int `json:"totalPlays" bson:"totalPlays"`
		PlayingNow int `json:"playingNow" bson:"playingNow"`
	} `json:"stats,omitempty"`

	// Dates
	DateLastModified time.Time `json:"lastModifiedDate" bson:"lastModifiedDate"`
	DateCreated      time.Time `json:"createdDate,omitempty" bson:"createdDate"`

	// Publisher
	Publisher struct {
		UID      string `json:"uid" bson:"uid"`
		Username string `json:"username" bson:"username"`
	} `json:"publisher" bson:"publisher"`

	// App
	App struct {
		UID    string `json:"uid" bson:"uid"`
		AID    string `json:"aid" bson:"aid"`
		WebURL string `json:"webURL,omitempty" bson:"webURL"`
	} `json:"app" bson:"app"`
}

// Games is a list of Game
type Games []Game

// GameUserBanHistory is a history of the Users who have been banned from a Game
type GameUserBanHistory struct {
	GameUID string                            `json:"gameUID" bson:"gameUID"`
	History map[string]GameUserBanHistoryItem `json:"history" bson:"history"`
}

type GameUserBanHistoryItem struct {
	UserUID   string `json:"userUID" bson:"userUID"`
	DateAdded string `json:"dateAdded" bson:"dateAdded"`
}

// GameUserPurchaseHistory is a history of the Users who have purchased a Game
type GameUserPurchaseHistory struct {
	GameUID string                                 `json:"gameUID" bson:"gameUID"`
	History map[string]GameUserPurchaseHistoryItem `json:"history" bson:"history"`
}

type GameUserPurchaseHistoryItem struct {
	UserUID   string         `json:"userUID" bson:"userUID"`
	DateAdded time.Time      `json:"dateAdded" bson:"dateAdded"`
	Receipt   PaymentReceipt `json:"receipt" bson:"receipt"`
}

type GameMedia struct {
	Thumbnail struct {
		UID    string `json:"uid" bson:"uid"` // uid of Media
		Source string `json:"source" bson:"source"`
	} `json:"thumbnail,omitempty" bson:"thumbnail"`
	Images map[string]struct {
		UID    string `json:"uid" bson:"uid"`
		Source string `json:"source" bson:"source"`
	} `json:"images,omitempty" bson:"images"`
	Videos map[string]struct { // { "uid": { "thumbnail": "", "source": ""}}
		Thumbnail string `json:"thumbnail" bson:"thumbnail"`
		Source    string `json:"source" bson:"source"`
	} `json:"videos,omitempty" bson:"videos"`
}

type GameSystemSupport struct {
	Browser GameBrowserSupport `json:"browser" bson:"browser"`
}

// e.g. { "chrome": { "desktop": true, "mobile": false }}
type GameBrowserSupport map[string]GameBrowserSupportItem

type GameBrowserSupportItem struct {
	Desktop bool `json:"desktop"`
	Mobile  bool `json:"mobile"`
}

// e.g. { "english": { "interface": true, "audio": true, "subtitles": false }}
type GameLanguageSupport map[string]struct {
	Interface bool `json:"interface,omitempty"`
	Audio     bool `json:"audio,omitempty"`
	Subtitles bool `json:"subtitles,omitempty"`
}

const (
	GameStatusActive    = "active"    // published
	GameStatusInactive  = "inactive"  // not published
	GameStatusSuspended = "suspended" // suspended
	GameStatusDeleted   = "deleted"   // deleted

	GamePrivacyPublic   = "public"
	GamePrivacyPrivate  = "private"
	GamePrivacyUnlisted = "unlisted"

	GameBrowserChrome  = "chrome"
	GameBrowserFirefox = "firefox"
	GameBrowserSafari  = "safari"
	GameBrowserEdge    = "edge"
	GameBrowserIE      = "ie"
)
