package domain

import "time"

type Tag struct {
	UID   string `json:"uid" bson:"uid"`
	TID   string `json:"tid" bson:"tid"`
	Alias string `json:"alias" bson:"alias"`

	GameSuggestHistory map[string]GameSuggestItem `json:"gameSuggestHistory" bson:"gameSuggestHistory"` // Games suggested to have this Tag
}

type GameSuggestItem struct {
	GameUID            string                     `json:"gameUID" bson:"gameUID"`
	DateAdded          time.Time                  `json:"dateAdded" bson:"dateAdded"`
	UserSuggestHistory map[string]UserSuggestItem `json:"userSuggestHistory" bson:"userSuggestHistory"`
}

type UserSuggestItem struct {
	UserUID   string    `json:"userUID"`
	Type      string    `json:"type"` // e.g. publisher, admin, active user
	DateAdded time.Time `json:"dateAdded" bson:"dateAdded"`
}

const (
	UserSuggestTypePublisher = "publisher"
	UserSuggestTypeAdmin     = "admin"
	UserSuggestTypeActive    = "active"
)
