package domain

type GamePrice struct {
	Amount uint64 `json:"amount" bson:"amount"`
}

// Measured in cents
var PriceTiers = []uint64{0, 99, 199, 299, 399, 499, 699, 999, 1499, 1999}

func (price GamePrice) Valid() bool {
	var amountIsValid = false

	for _, t := range PriceTiers {
		if price.Amount == t {
			amountIsValid = true
			break
		}
	}

	return amountIsValid
}

func (price GamePrice) IsFree() bool {
	return price.Amount == 0
}
