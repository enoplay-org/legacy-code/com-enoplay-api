package domain

// e.g. { "keyboard": { "w": "Move forward", "a": "Turn left"}, "mouse": { "l_click": "Shoot" }}
type GameControl map[string]string
type GameControls map[string]GameControl

const KeyboardControl = "keyboard"
const MouseControl = "mouse"
const GamepadControl = "gamepad"

func (controls GameControls) Valid() bool {
	if controls[KeyboardControl] != nil && !controls.keyboardControlsValid() {
		return false
	}

	if controls[MouseControl] != nil && !controls.mouseControlsValid() {
		return false
	}

	return true
}

var KeyboardInput = map[string]int{"shift": 1, "alt": 1, "ctrl": 1, "cmd": 1, "1": 1, "2": 1, "3": 1, "4": 1, "5": 1, "6": 1, "7": 1, "8": 1, "9": 1, "0": 1, "a": 1, "b": 1, "c": 1, "d": 1, "e": 1, "f": 1, "g": 1, "h": 1, "i": 1, "j": 1, "k": 1, "l": 1, "m": 1, "n": 1, "o": 1, "p": 1, "q": 1, "r": 1, "s": 1, "t": 1, "u": 1, "v": 1, "w": 1, "x": 1, "y": 1, "z": 1}

func (controls GameControls) keyboardControlsValid() bool {
	for key := range controls[KeyboardControl] {
		if KeyboardInput[key] == 0 {
			return false
		}
	}
	return true
}

var MouseInput = map[string]int{"click_left": 1, "click_right": 1, "click_scroll": 1, "scroll_up": 1, "scroll_down": 1}

func (controls GameControls) mouseControlsValid() bool {
	for key := range controls[MouseControl] {
		if MouseInput[key] == 0 {
			return false
		}
	}
	return true
}
