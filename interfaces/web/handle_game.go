package web

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/go-zoo/bone"

	"gitlab.com/enoplay/com-enoplay-api/domain"
)

type GetGameResponseV0 struct {
	Game    domain.Game `json:"game,omitempty"`
	Message string      `json:"message"`
	Success bool        `json:"success"`
}

type CreateGameRequestV0 struct {
	Title       string           `json:"title"`
	Description string           `json:"description"`
	Price       domain.GamePrice `json:"price"`
}

type CreateGameResponseV0 struct {
	Game    domain.Game `json:"game,omitempty"`
	Message string      `json:"message"`
	Success bool        `json:"success"`
}

type ListGamesResponseV0 struct {
	Games   domain.Games `json:"games"`
	Message string       `json:"message,omitempty"`
	Success bool         `json:"success"`
}

type UpdateGameTitleRequestV0 struct {
	Title string `json:"title"`
}

type UpdateGameTitleResponseV0 struct {
	Game    domain.Game `json:"game,omitempty"`
	Message string      `json:"message,omitempty"`
	Success bool        `json:"success"`
}

type UpdateGameDescriptionRequestV0 struct {
	Description string `json:"description"`
}

type UpdateGameDescriptionResponseV0 struct {
	Game    domain.Game `json:"game,omitempty"`
	Message string      `json:"message,omitempty"`
	Success bool        `json:"success"`
}

type UpdateGamePriceRequestV0 struct {
	Price domain.GamePrice `json:"price"`
}

type UpdateGamePriceResponseV0 struct {
	Game    domain.Game `json:"game,omitempty"`
	Message string      `json:"message,omitempty"`
	Success bool        `json:"success"`
}

type UpdateGamePrivacyRequestV0 struct {
	Privacy string `json:"privacy"`
}

type UpdateGamePrivacyResponseV0 struct {
	Game    domain.Game `json:"game,omitempty"`
	Message string      `json:"message,omitempty"`
	Success bool        `json:"success"`
}

type UpdateGameBrowserSupportRequestV0 struct {
	Support domain.GameBrowserSupport `json:"support"`
}

type UpdateGameBrowserSupportResponseV0 struct {
	Game    domain.Game `json:"game,omitempty"`
	Message string      `json:"message,omitempty"`
	Success bool        `json:"success"`
}

type DeleteGameResponseV0 struct {
	Message string `json:"message,omitempty"`
	Success bool   `json:"success"`
}

type GameInteractor interface {
	Get(uid string) (domain.Game, error)
	GetByGID(gid string) (domain.Game, error)
	Create(user domain.User, game domain.Game) (domain.Game, error)
	ListGames(user domain.User, queryList map[string]interface{}, limit int, sort string) domain.Games

	UpdateTitle(game domain.Game, title string) (domain.Game, error)
	UpdateDescription(game domain.Game, description string) (domain.Game, error)
	UpdatePrice(game domain.Game, price domain.GamePrice) (domain.Game, error)
	UpdatePrivacy(game domain.Game, privacy string) (domain.Game, error)
	UpdateBrowserSupport(game domain.Game, support domain.GameBrowserSupport) (domain.Game, error)

	Delete(sessionToken string, game domain.Game) error
}

// GetGame retrieves a game by its gid
// If the game is private, the user must be the publisher
func (handler *Handler) GetGame(res http.ResponseWriter, req *http.Request) {
	// Retrieve User if available
	userUID := req.Context().Value(UserUIDContextKey)

	var user domain.User
	if userUID != nil {
		user, _ = handler.UserInteractor.Get(userUID.(string))
	}
	gid := bone.GetValue(req, "gid")

	// Retrieve Game
	game, err := handler.GameInteractor.GetByGID(gid)
	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusBadRequest, err.Error())
		return
	}

	// Validate User has permission to Retrieve Game
	if game.Privacy == domain.GamePrivacyPrivate {
		if user.UID == "" || game.Publisher.UID != user.UID {
			handler.util.renderer.Error(res, req, http.StatusUnauthorized, fmt.Sprintf("Unauthorized request"))
			return
		}
	}

	// Respond to request
	handler.util.renderer.Render(res, req, http.StatusOK, GetGameResponseV0{
		Game:    game,
		Message: "Game retrieved",
		Success: true,
	})
}

// CreateGame creates a new Game.
// Title, description and price are required fields in the body of the request.
func (handler *Handler) CreateGame(res http.ResponseWriter, req *http.Request) {
	userUID := req.Context().Value(UserUIDContextKey)
	if userUID == nil {
		handler.util.renderer.Error(res, req, http.StatusUnauthorized, "Requires authentication.")
		return
	}

	var body CreateGameRequestV0
	err := handler.util.DecodeRequestBody(req, &body)
	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusBadRequest, fmt.Sprintf("Request body parse error: %v", err.Error()))
		return
	}

	user, err := handler.UserInteractor.Get(userUID.(string))
	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusUnauthorized, "Error retrieving user")
		return
	}

	newGame := domain.Game{
		Title:       body.Title,
		Description: body.Description,
		Price:       body.Price,
	}

	game, err := handler.GameInteractor.Create(user, newGame)

	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusBadRequest, err.Error())
		return
	}

	handler.util.renderer.Render(res, req, http.StatusCreated, CreateGameResponseV0{
		Game:    game,
		Message: "Game created",
		Success: true,
	})
}

// ListGames returns a list of Games.
func (handler *Handler) ListGames(res http.ResponseWriter, req *http.Request) {
	userUID := req.Context().Value(UserUIDContextKey)

	var user domain.User
	if userUID != nil {
		user, _ = handler.UserInteractor.Get(userUID.(string))
	}
	// Setup input
	field := req.FormValue("field")
	fieldQuery := req.FormValue("q")
	perPageStr := req.FormValue("per_page")
	sort := req.FormValue("sort")

	device := req.FormValue("device")
	browser := req.FormValue("browser")

	perPage, err := strconv.Atoi(perPageStr)
	if err != nil {
		perPage = 20
	}

	// List of properties to search for
	queryList := make(map[string]interface{})
	queryList["field"] = field
	queryList["fieldQuery"] = fieldQuery
	queryList["browser"] = browser
	queryList["device"] = device

	games := handler.GameInteractor.ListGames(user, queryList, perPage, sort)

	handler.util.renderer.Render(res, req, http.StatusOK, ListGamesResponseV0{
		Games:   games,
		Message: "Game list retrieved",
		Success: true,
	})
}

// UpdateGameTitle updates the title of a Game.
func (handler *Handler) UpdateGameTitle(res http.ResponseWriter, req *http.Request) {
	userUID := req.Context().Value(UserUIDContextKey)
	if userUID == nil {
		handler.util.renderer.Error(res, req, http.StatusUnauthorized, "Requires authentication.")
		return
	}

	var body UpdateGameTitleRequestV0
	err := handler.util.DecodeRequestBody(req, &body)
	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusBadRequest, fmt.Sprintf("Request body parse error: %v", err.Error()))
		return
	}

	gid := bone.GetValue(req, "gid")
	game, err := handler.GameInteractor.GetByGID(gid)
	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusBadRequest, err.Error())
		return
	}

	if game.Publisher.UID != userUID {
		handler.util.renderer.Error(res, req, http.StatusUnauthorized, fmt.Sprintf("Unauthorized request"))
		return
	}

	game, err = handler.GameInteractor.UpdateTitle(game, body.Title)
	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusBadRequest, err.Error())
		return
	}

	handler.util.renderer.Render(res, req, http.StatusOK, UpdateGameTitleResponseV0{
		Game:    game,
		Message: "Game title updated",
		Success: true,
	})
}

// UpdateGameDescription updates the description of a Game
func (handler *Handler) UpdateGameDescription(res http.ResponseWriter, req *http.Request) {
	userUID := req.Context().Value(UserUIDContextKey)
	if userUID == nil {
		handler.util.renderer.Error(res, req, http.StatusUnauthorized, "Requires authentication.")
		return
	}

	var body UpdateGameDescriptionRequestV0
	err := handler.util.DecodeRequestBody(req, &body)
	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusBadRequest, fmt.Sprintf("Request body parse error: %v", err.Error()))
		return
	}

	gid := bone.GetValue(req, "gid")
	game, err := handler.GameInteractor.GetByGID(gid)
	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusBadRequest, err.Error())
		return
	}

	if game.Publisher.UID != userUID {
		handler.util.renderer.Error(res, req, http.StatusUnauthorized, fmt.Sprintf("Unauthorized request"))
		return
	}

	game, err = handler.GameInteractor.UpdateDescription(game, body.Description)
	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusBadRequest, err.Error())
		return
	}

	handler.util.renderer.Render(res, req, http.StatusOK, UpdateGameDescriptionResponseV0{
		Game:    game,
		Message: "Game description updated",
		Success: true,
	})
}

// UpdateGamePrice updates the price of a Game.
func (handler *Handler) UpdateGamePrice(res http.ResponseWriter, req *http.Request) {
	userUID := req.Context().Value(UserUIDContextKey)
	if userUID == nil {
		handler.util.renderer.Error(res, req, http.StatusUnauthorized, "Requires authentication.")
		return
	}

	var body UpdateGamePriceRequestV0
	err := handler.util.DecodeRequestBody(req, &body)
	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusBadRequest, fmt.Sprintf("Request body parse error: %v", err.Error()))
		return
	}

	gid := bone.GetValue(req, "gid")
	game, err := handler.GameInteractor.GetByGID(gid)
	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusBadRequest, err.Error())
		return
	}

	if game.Publisher.UID != userUID {
		handler.util.renderer.Error(res, req, http.StatusUnauthorized, fmt.Sprintf("Unauthorized request"))
		return
	}

	game, err = handler.GameInteractor.UpdatePrice(game, body.Price)
	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusBadRequest, err.Error())
		return
	}

	handler.util.renderer.Render(res, req, http.StatusOK, UpdateGamePriceResponseV0{
		Game:    game,
		Message: "Game price updated",
		Success: true,
	})
}

// UpdateGamePrivacy updates the privacy of a Game.
func (handler *Handler) UpdateGamePrivacy(res http.ResponseWriter, req *http.Request) {
	userUID := req.Context().Value(UserUIDContextKey)
	if userUID == nil {
		handler.util.renderer.Error(res, req, http.StatusUnauthorized, "Requires authentication.")
		return
	}

	var body UpdateGamePrivacyRequestV0
	err := handler.util.DecodeRequestBody(req, &body)
	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusBadRequest, fmt.Sprintf("Request body parse error: %v", err.Error()))
		return
	}

	gid := bone.GetValue(req, "gid")
	game, err := handler.GameInteractor.GetByGID(gid)
	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusBadRequest, err.Error())
		return
	}

	if game.Publisher.UID != userUID {
		handler.util.renderer.Error(res, req, http.StatusUnauthorized, fmt.Sprintf("Unauthorized request"))
		return
	}

	game, err = handler.GameInteractor.UpdatePrivacy(game, body.Privacy)
	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusBadRequest, err.Error())
		return
	}

	handler.util.renderer.Render(res, req, http.StatusOK, UpdateGamePrivacyResponseV0{
		Game:    game,
		Message: "Game privacy updated",
		Success: true,
	})
}

func (handler *Handler) UpdateGameBrowserSupport(res http.ResponseWriter, req *http.Request) {
	userUID := req.Context().Value(UserUIDContextKey)
	if userUID == nil {
		handler.util.renderer.Error(res, req, http.StatusUnauthorized, "Requires authentication.")
		return
	}

	var body UpdateGameBrowserSupportRequestV0
	err := handler.util.DecodeRequestBody(req, &body)
	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusBadRequest, fmt.Sprintf("Request body parse error: %v", err.Error()))
		return
	}

	gid := bone.GetValue(req, "gid")
	game, err := handler.GameInteractor.GetByGID(gid)
	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusBadRequest, err.Error())
		return
	}

	if game.Publisher.UID != userUID {
		handler.util.renderer.Error(res, req, http.StatusUnauthorized, fmt.Sprintf("Unauthorized request"))
		return
	}

	game, err = handler.GameInteractor.UpdateBrowserSupport(game, body.Support)
	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusBadRequest, err.Error())
		return
	}

	handler.util.renderer.Render(res, req, http.StatusOK, UpdateGameBrowserSupportResponseV0{
		Game:    game,
		Message: "Game browser support updated",
		Success: true,
	})
}

// DeleteGame deletes a Game.
func (handler *Handler) DeleteGame(res http.ResponseWriter, req *http.Request) {
	userUID := req.Context().Value(UserUIDContextKey)
	if userUID == nil {
		handler.util.renderer.Error(res, req, http.StatusUnauthorized, "Requires authentication.")
		return
	}

	accessToken := req.Context().Value(AccessTokenContextKey)
	if accessToken == nil {
		handler.util.renderer.Error(res, req, http.StatusUnauthorized, "Requires authentication.")
		return
	}

	gid := bone.GetValue(req, "gid")
	game, err := handler.GameInteractor.GetByGID(gid)
	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusBadRequest, err.Error())
		return
	}

	if game.Publisher.UID != userUID {
		handler.util.renderer.Error(res, req, http.StatusUnauthorized, fmt.Sprintf("Unauthorized request"))
		return
	}

	err = handler.GameInteractor.Delete(accessToken.(string), game)
	if err != nil {
		handler.util.renderer.Error(res, req, http.StatusBadRequest, err.Error())
		return
	}

	handler.util.renderer.Render(res, req, http.StatusOK, DeleteGameResponseV0{
		Message: "Game deleted",
		Success: true,
	})
}
