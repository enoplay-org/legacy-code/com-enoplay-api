package web_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/enoplay/com-enoplay-api/domain"
	"gitlab.com/enoplay/com-enoplay-api/interfaces/web"

	"testing"
)

func TestWeb(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Web Suite")
}

const testUserUID string = "testUserUID"

var webHandler *web.Handler

var _ = BeforeSuite(func() {
	webHandler = web.NewHandler(&web.HandlerOptions{
		WwwHostURL:    "",
		MobileHostURL: "",
	}, &RecaptchaHandlerMock{})
	webHandler.SessionTokenInteractor = &SessionTokenInteractorMock{}
	webHandler.GameInteractor = &GameInteractorMock{}
})

type RecaptchaHandlerMock struct {
}

func (recap *RecaptchaHandlerMock) Verify(recaptcha string) bool {
	return true
}

type SessionTokenInteractorMock struct {
}

func (interactor *SessionTokenInteractorMock) VerifyAccess(token string) (string, error) {
	return "", nil
}

func (interactor *SessionTokenInteractorMock) VerifyRefresh(token string) (string, error) {
	return "", nil
}

func (interactor *SessionTokenInteractorMock) CreateAccess(refreshToken string) (string, error) {
	return "", nil
}

func (interactor *SessionTokenInteractorMock) CreateRefresh(usernameOrEmail, password string) (string, domain.User, error) {
	return "", domain.User{}, nil
}

func (interactor *SessionTokenInteractorMock) DeleteRefresh(token string) error {
	return nil
}

type GameInteractorMock struct {
}

func (interactor *GameInteractorMock) Get(uid string) (domain.Game, error) {
	return domain.Game{}, nil
}

func (interactor *GameInteractorMock) GetByGID(gid string) (domain.Game, error) {
	return domain.Game{}, nil
}

func (interactor *GameInteractorMock) Create(user domain.User, game domain.Game) (domain.Game, error) {
	return domain.Game{}, nil
}

func (interactor *GameInteractorMock) ListGames(user domain.User, queryList map[string]interface{}, limit int, sort string) domain.Games {
	return domain.Games{}
}

func (interactor *GameInteractorMock) UpdateTitle(game domain.Game, title string) (domain.Game, error) {
	return domain.Game{}, nil
}

func (interactor *GameInteractorMock) UpdateDescription(game domain.Game, description string) (domain.Game, error) {
	return domain.Game{}, nil
}

func (interactor *GameInteractorMock) UpdatePrice(game domain.Game, price domain.GamePrice) (domain.Game, error) {
	return domain.Game{}, nil
}

func (interactor *GameInteractorMock) UpdatePrivacy(game domain.Game, privacy string) (domain.Game, error) {
	return domain.Game{}, nil
}

func (interactor *GameInteractorMock) UpdateBrowserSupport(game domain.Game, support domain.GameBrowserSupport) (domain.Game, error) {
	return domain.Game{}, nil
}

func (interactor *GameInteractorMock) Delete(sessionToken string, game domain.Game) error {
	return nil
}
