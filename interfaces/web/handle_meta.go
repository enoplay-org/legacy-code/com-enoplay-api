package web

import "net/http"

type GetIndexResponseV0 struct {
	Message  string `json:"message,omitempty"`
	Versions string `json:"versions,omitempty"`
	Success  bool   `json:"success,omitempty"`
}

func (handler *Handler) GetIndex(res http.ResponseWriter, req *http.Request) {
	handler.util.renderer.Render(res, req, http.StatusOK, GetIndexResponseV0{
		Message:  "Welcome to api.enoplay.com",
		Versions: "Supported api versions: v0",
		Success:  true,
	})
}

func (handler *Handler) GetCertBotKey(res http.ResponseWriter, req *http.Request) {
	res.Write([]byte("place_certbot_key_here"))
}
