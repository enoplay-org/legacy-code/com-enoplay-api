package repository

import (
	"fmt"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"gitlab.com/enoplay/com-enoplay-api/domain"
)

const PlayTokenCollection = "plays"

type DbPlayTokenRepo struct {
	DbRepo
	TAOptions *TokenAuthorityOptions
}

func NewDbPlayTokenRepo(dbHandlers map[string]DbHandler, taOptions *TokenAuthorityOptions) *DbPlayTokenRepo {
	dbPlayTokenRepo := &DbPlayTokenRepo{}
	dbPlayTokenRepo.dbHandlers = dbHandlers
	dbPlayTokenRepo.dbHandler = dbHandlers["DbPlayTokenRepo"]
	dbPlayTokenRepo.TAOptions = taOptions
	return dbPlayTokenRepo
}

func (repo *DbPlayTokenRepo) Verify(tokenString string) (domain.PlayTokenClaim, error) {
	// Ensure token is valid
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		return repo.TAOptions.PublicSigningKey, nil
	})
	if err != nil {
		return domain.PlayTokenClaim{}, fmt.Errorf("Error parsing play token: %v", err)
	}

	var claim domain.PlayTokenClaim

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		claim.UserUID = claims["userUID"].(string)
		claim.GameUID = claims["gameUID"].(string)
		claim.IsAnon = claims["isAnon"].(bool)
		claim.DateExpired = time.Unix(int64(claims["dateExpired"].(float64)), 0)
		claim.DateIssued = time.Unix(int64(claims["dateIssued"].(float64)), 0)
		claim.JTI = claims["jti"].(string)
	} else {
		return domain.PlayTokenClaim{}, fmt.Errorf("Invalid play token")
	}

	// Ensure token has not expired
	if time.Now().Sub(claim.DateExpired).Seconds() > 0 {
		return domain.PlayTokenClaim{}, fmt.Errorf("Token has expired")
	}

	return claim, nil
}

func (repo *DbPlayTokenRepo) Create(claim domain.PlayTokenClaim) (string, error) {
	claims := make(jwt.MapClaims)
	claims["userUID"] = claim.UserUID
	claims["gameUID"] = claim.GameUID
	claims["isAnon"] = claim.IsAnon
	claims["dateExpired"] = time.Now().Add(time.Minute * 10).Unix() // 10 minutes
	claims["dateIssued"] = time.Now().Unix()
	claims["jti"] = generateJTI()

	token := jwt.NewWithClaims(jwt.SigningMethodRS512, claims)

	return token.SignedString(repo.TAOptions.PrivateSigningKey)
}

func (repo *DbPlayTokenRepo) CreateAnon(claim domain.PlayTokenClaim) (string, error) {
	claims := make(jwt.MapClaims)
	claims["userUID"] = claim.UserUID
	claims["gameUID"] = claim.GameUID
	claims["isAnon"] = claim.IsAnon
	claims["dateExpired"] = time.Now().Add(time.Minute * 5).Unix() // 5 minutes
	claims["dateIssued"] = time.Now().Unix()
	claims["jti"] = generateJTI()

	token := jwt.NewWithClaims(jwt.SigningMethodRS512, claims)

	return token.SignedString(repo.TAOptions.PrivateSigningKey)
}
